var now_mode = 'pen';
var mycvs = document.getElementById("MyCanvas");
var ctx = mycvs.getContext("2d");
mycvs.style.cursor = "url('pencil.png') 0 0 , auto";
ctx.strokeStyle = "#000000";
ctx.lineWidth = 1;
var Drawing = false;
var lastX = 0;
var lastY = 0;

var startingText = 0;

var imgData;
var file_uploaded = new Image();

var now_color = "#000000";
var now_width = 1;
function fontSetting(){
    var now_font = document.getElementById("textfont").value;
    var now_fontsize = document.getElementById("textsize").value;
    ctx.font = now_fontsize + "px " + now_font; 
}
ctx.font = "30px Arial";
ctx.fillStyle = "#000000";
ctx.lineJoin = 'round';
ctx.lineCap = 'round';

var flag = false; // if fill is true;

var hue = 0;
//saveState();
mycvs.addEventListener('mousemove', drawsth);
mycvs.addEventListener('mouseup', () => {Drawing = false;});
mycvs.addEventListener('mouseout', () => Drawing = false);
mycvs.addEventListener('mousedown', (e) => {
    Drawing = true;
    lastX = e.offsetX;
    lastY = e.offsetY;
} );
mycvs.addEventListener('click', (e)=>{
    imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);
    PushImage();
    if(now_mode=="text"){
        var t = document.getElementById("textbox").value;
        ctx.fillText(t, e.offsetX, e.offsetY);
    }
});

/* line width change */
function lineWidthChange(){
    var w = document.getElementById("lineWidthInput").value;
    now_width = w;
}
/* text function for backspace */

/* undo redo function */
var PushArray = new Array();//used to save image at any state
var Steps = -1;//initialize step
var mycvsImage;
function PushImage(){
    Steps++;
    if(Steps < PushArray.length){PushArray.length = Steps;}
    PushArray.push(mycvs.toDataURL());
}
function undo(){
    if(Steps >= 0){
        Steps--;
        mycvsImage = new Image();
        mycvsImage.src = PushArray[Steps];
        ctx.clearRect(0, 0, mycvs.width, mycvs.height);/* 清空給定矩形內的指定像素 */
        mycvsImage.onload = function(){ctx.drawImage(mycvsImage, 0, 0);imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height)}
        ;
    }
}
function redo(){
    if(Steps < PushArray.length-1){
        Steps++;
        mycvsImage = new Image();
        mycvsImage.src = PushArray[Steps];
        ctx.clearRect(0, 0, mycvs.width, mycvs.height);/* 清空給定矩形內的指定像素 */
        mycvsImage.onload = function(){ctx.drawImage(mycvsImage, 0, 0);imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);}
        
    }
}
function downloadImage(el){
    var cvsimage = mycvs.toDataURL("image/jpg");
    el.href = cvsimage;
}
function uploadImage(){
    var fileinput = document.getElementById('upload').files[0];
    var reader = new FileReader();
    reader.readAsDataURL(fileinput);
    reader.onload = function(e){
        var image = new Image();
        image.src = e.target.result;
        ctx.drawImage(image, 50, 50);
    }
    imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);
    PushImage();
}

function draw_pen(){
    now_mode = 'pen';
    mycvs.style.cursor = "url('pencil.png') 0 -30, auto";
}
function erase(){
    now_mode = 'erase';
    mycvs.style.cursor = "";
}
function cir(){
    now_mode = "cir";
    imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);
    mycvs.style.cursor = "";
}
function rec(){
    now_mode = 'rec';
    imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);
    mycvs.style.cursor = "";
}
function tri(){
    now_mode = 'tri';
    imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);
    mycvs.style.cursor = "";
}
function draw_text(){
    now_mode = 'text';
    fontSetting();
    mycvs.style.cursor = "";
}
function fillshape(){
    if(flag == true) flag = false;
    else flag = true;
    mycvs.style.cursor = "";
}
function rainbow(){
    now_mode = 'rainbow';
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    hue++;
    if(hue >= 360)hue = 0;
    mycvs.style.cursor = "";
}
function clearcvs(){
    ctx.clearRect(0, 0, mycvs.width, mycvs.height);/* 清空給定矩形內的指定像素 */
    mycvs.style.cursor = "";
    imgData = ctx.getImageData(0, 0, mycvs.width, mycvs.height);
    PushImage();
}
function drawsth(e){
    if(!Drawing) return;
    if(now_mode == 'pen'){
        ctx.strokeStyle = now_color;
        ctx.lineWidth = now_width;
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();

        lastX = e.offsetX;
        lastY = e.offsetY;
    }
    else if(now_mode == 'erase'){
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";/* 顏色設定為背景顏色，透明效果 製作橡皮擦功能 */
        ctx.lineWidth = now_width;
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
        ctx.restore();

        lastX = e.offsetX;
        lastY = e.offsetY;
    }
    else if(now_mode == 'cir'){
        ctx.strokeStyle = now_color;
        ctx.fillStyle = now_color;
        ctx.lineWidth = now_width;
        ctx.clearRect(0, 0, mycvs.width, mycvs.height);/* 清空給定矩形內的指定像素 */
        ctx.putImageData(imgData, 0, 0);
        ctx.beginPath();
        ctx.arc(lastX, lastY, Math.sqrt(Math.pow(e.offsetX-lastX,2)+Math.pow(e.offsetY-lastY,2)), 0, Math.PI*2, true);
        if(!flag) ctx.stroke();
        else ctx.fill();
    }
    else if(now_mode == 'rec'){
        ctx.strokeStyle = now_color;
        ctx.fillStyle = now_color;
        ctx.lineWidth = now_width;
        ctx.clearRect(0, 0, mycvs.width, mycvs.height);/* 清空給定矩形內的指定像素 */
        ctx.putImageData(imgData, 0, 0);
        ctx.beginPath();
        ctx.rect(lastX, lastY, e.offsetX-lastX, e.offsetY-lastY);
        if(!flag) ctx.stroke();
        else ctx.fill();
    }
    else if(now_mode == "tri"){
        ctx.strokeStyle = now_color;
        ctx.fillStyle = now_color;
        ctx.lineWidth = now_width;
        ctx.clearRect(0, 0, mycvs.width, mycvs.height);/* 清空給定矩形內的指定像素 */
        ctx.putImageData(imgData, 0, 0);
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(lastX-(e.offsetX-lastX), e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.closePath();
        if(!flag) ctx.stroke();
        else ctx.fill();
    }
    else if(now_mode == 'rainbow'){
        ctx.lineWidth = now_width;
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();

        lastX = e.offsetX;
        lastY = e.offsetY;
        rainbow();
    }
   
}

/* color selector */
var colorBlock = document.getElementById('color-selector');
var ctx1 = colorBlock.getContext("2d");
var width1 = colorBlock.width;
var height1 = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctx2 = colorStrip.getContext("2d");
var width2 = colorStrip.width;
var height2 = colorStrip.height;

var x = 0;
var y = 0;
var drag = false;
var rgbaColor = 'rgba(255,0,0,1)';

ctx1.rect(0, 0, width1, height1);
fillGradient();

ctx2.rect(0, 0, width2, height2);
var grd1 = ctx2.createLinearGradient(0, 0, 0, height1);
grd1.addColorStop(0, 'rgba(255, 0, 0, 1)');
grd1.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
grd1.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
grd1.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
grd1.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
grd1.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
grd1.addColorStop(1, 'rgba(255, 0, 0, 1)');
ctx2.fillStyle = grd1;
ctx2.fill();

function click(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx2.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  fillGradient();
}

function fillGradient() {
  ctx1.fillStyle = rgbaColor;
  ctx1.fillRect(0, 0, width1, height1);

  var grdWhite = ctx2.createLinearGradient(0, 0, width1, 0);
  grdWhite.addColorStop(0, 'rgba(255,255,255,0)');
  grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
  ctx1.fillStyle = grdWhite;
  ctx1.fillRect(0, 0, width1, height1);

  var grdBlack = ctx2.createLinearGradient(0, 0, 0, height1);
  grdBlack.addColorStop(0, 'rgba(0,0,0,0)');
  grdBlack.addColorStop(1, 'rgba(0,0,0,1)');
  ctx1.fillStyle = grdBlack;
  ctx1.fillRect(0, 0, width1, height1);
}

function mousedown(e) {
  drag = true;
  changeColor(e);
}

function mousemove(e) {
  if (drag) {
    changeColor(e);
  }
}

function mouseup(e) {
  drag = false;
}

function changeColor(e) {
  x = e.offsetX;
  y = e.offsetY;
  var imageData = ctx1.getImageData(x, y, 1, 1).data;
  rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
  now_color = rgbaColor;
  ctx.fillStyle = rgbaColor;
}

colorStrip.addEventListener("click", click, false);
//colorblock.addEventListener("click", click, false);
colorBlock.addEventListener("mousedown", mousedown, false);
colorBlock.addEventListener("mouseup", mouseup, false);
colorBlock.addEventListener("mousemove", mousemove, false);

// background of window

