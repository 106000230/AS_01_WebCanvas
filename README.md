# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
**Canvas區域區分**
![image](canvas.PNG)
-Canvas 畫布本身  
-Sidebar 功能選擇  
-Color-selector 顏色選擇  
分別用css將定義區域畫上border，SideBar部分是用虛線border構成，color-selector的border是用"outset"的style。  
background-color我選擇了漸層的寫法，background-image: linear-gradient(midnightblue,black);。  
   
**Canvas input button**  
用一個全域變數now_mode來存放當下所選擇的工具（default值為'pen')，當button的onclick事件被trigger的時候將會改變now_mode值  
-pen(單純的根據滑鼠游標畫出線條)  
-eraser(擦除Canvas上的東西)  
-text(用來打字放上canvas)  
-rectangle(拉出長方圖形)  
-circle(拉出圓形)  
-triangle(拉出三角形)  
-rainbow(畫出彩虹的線條)  
-undo / redo  
-fillshape(按下一次將會改變fill的值，true->false or false->true)  
-reset(清除畫布)  
-download(按下之後會將當下canvas存成一個圖檔)  
-file, upload(點擊file選擇要上傳的檔案，在點及兩次upload將圖案畫上canvas)
    
**Canvas功能實作**  
var mycvs = document.getElementById("MyCanvas");  
var ctx = mycvs.getContext("2d");  
在剛開始要用上面的function去取得2D的渲染環境，進而在上面畫出圖形。  
  
ctx.strokeStyle = "#000000";  
ctx.lineWidth = 1;  
var Drawing = false;  
ctx.font = "30px Arial";  
ctx.fillStyle = "#000000";  
  
ctx.lineJoin = 'round';  
ctx.lineCap = 'round';  
程式一開始的全域變數是用來儲存一些需要變動的數值，根據不同功能變換需更換的數值，開始進入畫面時會先設定default值。從color-selector上選擇顏色後改變strokeStyle(for drawing)和fillStyle(for texting)，若從selector選擇不同字形與字的大小將會改變font的值，若用拉桿改變粗細會改變lineWidth的值。而最後兩個lineJoin與lineCap則是讓線條變得圓滑。  
在event listener 的部分我分以下幾個狀態來設定drawing值:  
mousedown drawing狀態設為true  
mouseup drawing狀態設為false  
mousemove call draw function ()   
mouseout drawing 狀態設為false  
我的作法是只要在canvas上move就不斷call function，再進到function判斷有沒有在drawing狀態，以及now_mode是什麼功能，根據now_mode的值去決定在畫布上做的動作 
   
**Pen**  
![image](pen.png)  
功能:  
點擊pen圖案之後，在canvas上按住拖曳擊可畫出圖案，顏色可由color-selector選擇，線條粗細可由拉桿調整。  
實作過程:  
ctx.beginPath(); // 用來新增繪圖  
ctx.moveTo(lastX, lastY); // 設定圖形開始的點  
ctx.lineTo(e.offsetX,e.offsetY); // 開始拉圖形   
ctx.stroke(); // 將設定好的圖形畫上Canvas  
利用不斷往游標位置畫線的方式，因為mousemove&&mousedown觸發不斷被呼叫drawsth function，將會畫出一條滑鼠畫過的線。  
  
**Eraser**  
![image](eraser.png)  
功能:  
點擊eraser圖案之後，在canvas上按住拖曳擊可畫出圖案，顏色是設為canvas原始背景圖案，橡皮擦粗細可由拉桿調整。  
實作過程:  
這個function內的內容與pen差不多相同，但加入以下的funciton call將顏色設為與背景顏色相同，且畫之前save狀態，畫之後回復顏色設定狀態，如此以來便可以不被這次的設定影響，點選其他功能時仍能回復原始顏色。  
ctx.save();  
ctx.globalCompositeOperation = "destination-out";  
/other function call to draw line/  
ctx.restore();//畫完後在回覆畫之前的狀態  

**Text**  
![image](text.png)  
功能:  
 1.點選Sidebar中text input的格子打上欲畫上畫布的字，2.再點擊text的圖形，3.在Canvas上面欲放上字體的位置點擊(可在多個位置放置)4.完成!  
同樣的可從color-selector選擇字體的顏色，從selector選擇字形  
實作過程:  
我用一個input text的格子去紀錄要放上的text內容，用以下兩個程式碼取值以及畫上畫布  
var t = document.getElementById("textbox").value;  
ctx.fillText(t, e.offsetX, e.offsetY);  
selector的部分，字體放入了三種(標楷體 新細明體 微軟正黑體)用value來取值，字體大小分成7種(12/14/16/18/20/22/24)用value來取值，最後用一個陣列存入font，每次選取顏色以及改變selector值的時候就會call這個function。  
function fontSetting(){  
　var now_font = document.getElementById("textfont").value;  
　var now_fontsize = document.getElementById("textsize").value;  
　ctx.font = now_fontsize + "px " + now_font;  
}  

**Circle/Rectangle/Triangle (Fill/notFill)**  
![image](circle.png) ![image](rectangle.png) ![image](triangle.png) ![image](fill.png)  
功能:  
點選一次fill即可從空心變實心或從實心變空心，點選圖形之後在畫布上按下拖曳擊可畫出所選的圖形，顏色可由color-selector選擇，線條粗細可由拉桿調整。  
實作過程:  
這個function一開始花我很多時間debug，圖案在不斷call function之下將所有走過的路徑全部畫了圖案，無法在放開時只畫出一個圖案
有一個方案是在mouseup時才畫，但這樣做出的效果十分不自然，於是想了許久當我在做完clear畫布的reset function之後想到了這個作法，每一次要畫之前都先clear畫布，再將前一狀態所存下的imgData放上畫布，然後再畫圖案，即可做出我想要的效果。而實心圖案的功能就是點一下就能變化實心或空心，作法就是在最後一個步驟有所不同而已。  
clearcvs();  
ctx.putImageData(imgData, 0, 0);  
ctx.beginPath();  
//不同圖案下面這個function call會不一樣  
畫圓形ctx.arc(lastX, lastY, Math.sqrt(Math.pow(e.offsetX-lastX,2)+Math.pow(e.offsetY-lastY,2)), 0, Math.PI*2, true);  
畫方形ctx.rect(lastX, lastY, e.offsetX-lastX, e.offsetY-lastY);  
畫三角形  
ctx.moveTo(lastX, lastY);  
ctx.lineTo(lastX-(e.offsetX-lastX), e.offsetY);  
ctx.lineTo(e.offsetX, e.offsetY);  
ctx.closePath();  
//flag用來看fill的狀態，選擇不同的function call畫出實心和空心的差別  
if(!flag) ctx.stroke();  
else ctx.fill();  
  
**Rainbow**  
![image](rainbow.png)  
功能:與pen相同，但顏色隨時間不斷改變，線條粗細可由拉桿調整。  
實作過程:  
這個function從助教的demo看到之後就很喜歡，於是就決定一定要把他做出來，開始做之後才發現其實根本不難，不斷call function的過程中將色環的數字不斷往上加，同時更新strokeStyle，做出一種隨時間變化的筆刷。  

**Undo/Redo**  
![image](undo.png) ![image](redo.png)  
功能:  
按下undo回上一層，按下redo到下一層。  
實作過程:  
這個function我的作法是用陣列來儲存每一次對畫布變更狀態時所存下的image，Push進到陣列，當按下undo的時候先判斷陣列當下的index值，若>0的話就可以抓出那個陣列element的值畫到畫布上，並同時更新imgData值（for畫圖案的function)，當按下redo時也對index值做判斷，若<(陣列長度-1)的話代表可以redo，這時候抓到陣列中對應index的值畫到畫布上，並同時更新imgData值即可。  
這個function是用來記錄每一次動作結束存下當下圖檔放入陣列中並更新index值。call function的時機我選擇用mouse在canvas上click的event去trigger，因為用mouseup,mousedown,mousemove都會多次call function，將會讓陣列亂掉，這也是一開始讓我很頭痛的地方，但用click trigger之後，會在mouse up的那一瞬間觸發一次function call，此時的undo redo即可正常運作。  
function PushImage(){  
　　Steps++;  
　　if(Steps < PushArray.length){PushArray.length = Steps;}  
　　PushArray.push(mycvs.toDataURL());  
}  

**Reset**  
![image](reset.png)  
功能:  
按下即可清空畫布。  
實作過程:  
利用這個function就可以清空對應到物件的方形範圍。  
ctx.clearRect(0, 0, mycvs.width, mycvs.height);  
  
**Download**  
![image](download.png)  
功能:  
按下擊可儲存圖檔。  
實作過程:  
利用mycvs.toDataURL("image/jpg")將物件canvas儲存成image jpg檔案。  
  
**File and Upload**  
![image](file.png) ![image](upload.png)  
功能:  
先點擊file圖案選取欲上傳的圖檔，再點擊兩次upload圖案將選取的圖檔放上canvas。  
實作過程:  
upload是一個input style="file"的物件，他選取的圖檔會被儲存在一個files的陣列中，透過一個FileReader來讀取丟入的檔案，並利用drawImage()，將圖檔放在(50,50)的位置。這裡我做了一個callback function去確認他是否已經load完成，若完成才做drawImage的部分。  
  
**Color selector**  
這裡因為一開始用了很陽春的寫法，覺得畫面不夠好看，所以我參考了網路上的做法，利用畫多個漸層的方式建立color-selector，再用mouse event去取他們的RGB data值，同時更新strokeStyle和fillStyle，且cursor由標放上color-selector時也會變成crosshair的style。  
  
**Cursor URL**  
![image](pencil.png) ![image](cross.png)  
兩個滑鼠變化:  
當滑鼠游標在now_mode == "pen"時也會改變游標圖案，我是用了pencil的圖案來當作url的source。  
當滑鼠游標在color-selector上，用了crosshair style。  
  
**心得**  
這次的作業花了我很多的時間查資料、debug，其中收穫最大的是慢慢熟悉了html css javascript，也做出了比網路上成品厲害很多的作品，成就感100分，熬了很多個夜算是值得了。  
